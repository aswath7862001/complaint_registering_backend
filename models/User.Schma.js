const mongoose = require('mongoose');

const user_model = new mongoose.Schema({
    username: {
        type: String,
        default: ''
    },
    otp: {
        type: Number,
        default: 0
    },
    address: {
        type: String,
        default: ''
    },
    phone: {
        type: Number,
        default: 0
    },
    email: {
        type: String,
        default: ''
    }
});

module.exports = mongoose.model("User.Schema",user_model)